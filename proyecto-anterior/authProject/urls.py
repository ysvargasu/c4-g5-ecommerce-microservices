from django.urls import path,include
from authApp.admin import router
from rest_framework_simplejwt.views import (TokenObtainPairView, TokenRefreshView)
from authApp import views

urlpatterns = [
  path('login/', TokenObtainPairView.as_view()),
  path('refresh/', TokenRefreshView.as_view()),
  path('user/', views.UserCreateView.as_view()),
  path('api/',include(router.urls)),
  path('user/<int:pk>/', views.UserDetailView.as_view()),
  path('api/<int:pk>/',views.ProductDetailView.as_view())
]