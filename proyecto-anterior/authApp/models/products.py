from django.db import models

class Products(models.Model):
    id = models.BigAutoField(primary_key=True)
    name = models.CharField('Name', max_length = 15, unique=True)    
    description = models.CharField('Description',max_length= 30)
    stock = models.IntegerField(default=0)
    price = models.IntegerField(default=0)