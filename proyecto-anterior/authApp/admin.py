from django.contrib import admin
from .models.user import User
from .models.account import Account
from rest_framework import routers
from authApp.views.productsCreateView import ProductCreateView

admin.site.register(User)
admin.site.register(Account)
router = routers.DefaultRouter()
router.register('products',ProductCreateView)