from .userCreateView import UserCreateView
from .userDetailView import UserDetailView
from .productsCreateView import ProductCreateView
from .productsDetailView import ProductDetailView