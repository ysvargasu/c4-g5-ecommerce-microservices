from rest_framework import generics
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from authApp.models.products import Products
from authApp.serializers.productsSerializer import ProductsSerializer
from authApp.serializers.userSerializer import UserSerializer

class ProductDetailView(generics.RetrieveAPIView):
    queryset = Products.objects.all()
    serializer_class = UserSerializer
    permission_classes = (IsAuthenticated,)

    def get(self,request,format=None):
        products = Products.objects.all()
        serializer = ProductsSerializer(products,many=True)
        return Response(serializer.data)