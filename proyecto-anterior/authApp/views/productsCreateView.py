from rest_framework import viewsets
from authApp.models import products
from authApp.serializers import productsSerializer

class ProductCreateView(viewsets.ModelViewSet):
    queryset = products.Products.objects.all()
    serializer_class = productsSerializer.ProductsSerializer