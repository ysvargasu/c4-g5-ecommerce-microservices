from rest_framework import serializers
from authApp.models.products import Products
from django.shortcuts import render

class ProductsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Products
        fields = "__all__"

    def to_representation(self, obj):
        products = Products.objects.get (id = obj.id)
        return {
            'id': products.id,
            'name': products.name,
            'stock': products.stock,
            'price': products.price,
            'description': products.description
        }
        
